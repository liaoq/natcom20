function mnet = cifar_conv_model_sm_10000(numClass)  
%numClass = numel(imdb.meta.classes);
%             32 16 8  4  1   1
featSize  = [ 3  16 16 16 16 numClass ];   
convSizes = [-1  3  3  3  4   1];
%convOpts = { [] {'stride',2,'pad',1} {'stride',2,'pad',1} {'stride',2,'pad',1} };  
convOpts = { [] };
for i = 2:numel(convSizes)
    if convSizes(i) == 3
       convOpts{i}  = {'stride',2,'pad',1};
    else
        convOpts{i} = {'stride',1,'pad',0};
    end
end
mnet = nnbox_init_mnet(featSize,convSizes,convOpts);

mnet.params(2).nonlinear = @identityFunc;
