function mnet = nnbox_init_mnet(featSize,convSizes,convOpts)
% featSize  = [ 3 16 32 64]; convSizes = [-1 3 3 3]; convOpts = { [] {'stride',2,'pad',1} {'stride',2,'pad',1} {'stride',2,'pad',1} };  
% 
params = struct;
init_scale = 0.05;
assert( numel(convSizes) == numel(featSize) );
for i = 2:numel(featSize)
    params(i).f = single( init_scale*randn(convSizes(i),convSizes(i),featSize(i-1),featSize(i)) );
    params(i).b = single( zeros(featSize(i), 1) );  
    params(i).moments = [];
end
layers = struct;
for i = 1:numel(params)
    layers(i).x = [];
    layers(i).dx = [];
    if i == 1 || i == 2 
        layers(i).nonlinear = @vl_linear;
    else
        layers(i).nonlinear = @vl_nnrelu;
    end
    layers(i).convOpts = convOpts{i};
    layers(i).layer_func = @nnbox_bn_nonlin_conv;
end

mnet.layers = layers;
mnet.params = params;




