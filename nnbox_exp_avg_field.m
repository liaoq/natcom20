function params = nnbox_exp_avg_field(ratio,params,fd,value) 
if ~isfield(params,fd) || isempty(params.(fd)) 
    params.(fd) = value;
else
    params.(fd) = ratio .* params.(fd) + (1-ratio) .* value;
end


