function mnet = nnbox_simple_fnn_fp_and_bp(mnet) 
    mnet.layers(1).x = mnet.x; 
    mnet = nnbox_simple_fnn(mnet,0);
    
    if ~isfield(mnet,'training_type') || strcmp(mnet.training_type,'classification')
        mnet.loss       = vl_nnloss(mnet.layers(end).x,mnet.y,[],'loss','softmaxlog');  
        mnet.top1err    = vl_nnloss(mnet.layers(end).x,mnet.y,[],'loss','topkerror','topK',1);
        mnet.layers(end).dx = vl_nnloss(mnet.layers(end).x,mnet.y, 1,'loss','softmaxlog');
        if isfield(mnet,'keep_error_pattern') && mnet.keep_error_pattern
            [~, pred] = max(mnet.layers(end).x,[],3);
            %            [~, gt]  = max(mnet.y,[],3);
            mnet.error_pattern = (pred(:) ~= mnet.y(:) );
        end
    elseif strcmp(mnet.training_type,'square_loss')
        mnet.layers(end).dx = mnet.layers(end).x - mnet.y;
         % sum(mnet.layers(end).dx(:).^2) .* 1/2;
        mnet.loss = sum(sum(mnet.layers(end).dx.^2,3).^(1/2)); 
    elseif strcmp(mnet.training_type,'square_loss_ecoc')  
        mnet.layers(end).dx = mnet.layers(end).x - label_1D_to_ecoc_4D(mnet.y,mnet.numClass);  
        %mnet.loss = sum(mnet.layers(end).dx(:).^2) .* 1/2; 
        mnet.loss = sum(sum(mnet.layers(end).dx.^2,3).^(1/2));  
        mnet.top1err    = vl_nnloss(mnet.layers(end).x,mnet.y,[],'loss','topkerror','topK',1); 
        if isfield(mnet,'keep_error_pattern') && mnet.keep_error_pattern
            [~, pred] = max(mnet.layers(end).x,[],3);
            %            [~, gt]  = max(mnet.y,[],3);
            mnet.error_pattern = (pred(:) ~= mnet.y(:) );
            mnet.loss_pattern  = sum(mnet.layers(end).dx.^2,3).^(1/2);
            mnet.loss_pattern  = mnet.loss_pattern(:);
        end 
    end
    
    mnet = nnbox_simple_fnn(mnet,1); 
    
    
    
    
