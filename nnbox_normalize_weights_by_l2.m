function mnet = nnbox_normalize_weights_by_l2(mnet,ratio)
% mnet_2 = mnet; mnet_2 = nnbox_absorb_bn(mnet_2);  mnet_2 = nnbox_normalize_weights_by_l2(mnet_2,1);     
% mnet.no_bn = 1;
product = 1;
for i = 1:numel(mnet.params)
    if  i >= 2     
        the_norm = l2norm(mnet.params(i).f(:),1);           
        % mnet.params(i).f = mnet.params(i).f./the_norm;
        % mnet.params(i).b = mnet.params(i).b./the_norm;         
        %the_norm = ratio;  
        mnet.params(i).f = mnet.params(i).f./the_norm; 
        product = product * the_norm;       
        mnet.params(i).b = mnet.params(i).b./product;       
        % end     
    end
end


% conv({},{})
% conv({A},{B},{C})
% conv(

