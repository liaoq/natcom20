function params = monnlinal_empty_dw(params) 
% empty all fields start with d
fds = fieldnames(params);
for i = 1:numel(fds)
    if strcmp(fds{i}(1), 'd') 
        params.(fds{i}) = [];
    end
end


