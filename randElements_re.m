function re = randElements_re(M,k)
y = randsample(numel(M),k,true);
re = M(y);
