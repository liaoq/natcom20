function  print_jpg_new_full_screen (filename,handle)
mkdirFile(filename);
if ~exist('handle','var') || isempty(handle)
    % TI = get(gca,'TightInset');
    % OP = get(gca,'OuterPosition');
    % Pos = OP + [ TI(1:2), -TI(1:2)-TI(3:4) ];
    % set( gca,'Position',Pos);
    %fig = figure(1);
    %set (gcf, 'Units', 'normalized', 'Position', [0,0,1,1]);
    set(gcf,'PaperUnits','normalized','PaperPosition',[0 0 1 0.5])
%    ti = get(gca,'TightInset')
%    set( gca,'Position',ti);

    print( gcf, '-djpeg', filename);    
else
    print( handle, '-djpeg', filename);    
end



