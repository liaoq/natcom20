function mnet = nnbox_simple_fnn(mnet,bprop_p)
layers = mnet.layers;
params = mnet.params;

opts = struct;

if ~bprop_p
    for i = 2:numel(layers)
        %opts = mnet;
        layer_func = getParam( layers(i), 'layer_func', @nnbox_bn_nonlin_conv);
        % if i == 2
        %     opts.no_bn = 0; % the first one always normalizes
        % end 
        [mnet layers params] = layer_func(mnet,layers,params,i,bprop_p); 
    end
else
    for i = numel(layers):-1:2
        layer_func = getParam( layers(i), 'layer_func', @nnbox_bn_nonlin_conv); 
        [mnet layers params] = layer_func(mnet,layers,params,i,bprop_p);
        %params(i) = nnbox_exp_avg_field(mnet.moments_avg_ratio,params(i),'moments',moments);
    end
end


mnet.layers = layers;
mnet.params = params;

