function num = nnbox_count_num_params(mnet) 
fds = {'f','b'};  
num = [];
for i = 1:numel(mnet.params)
    for f = 1:numel(fds)
        num(i,f) = numel(mnet.params(i).(fds{f}));
    end
end

