function re =  l2norm(X,kdim)
re = sqrt(nansum(X.^2,kdim));
