function ecoc = label_1D_to_ecoc_4D(label,numClass) 
    label = reshape(label,[1 1 1 numel(label)]);
    if ~exist('numClass','var')
        numClass = max(label(:));
    end
    ecoc  = ones(1,1,numClass,size(label,4));
    ecoc  = cumsum(ecoc,3);
    ecoc  = bsxfun(@eq,ecoc,label); 
    

