function params = monnlinal_update_w_bs(params,lr,batchsize,fds)
%fds = {'w_g', 'w0_g', 'w', 'w0', 'w2', 'bn_G', 'bn_B'}; 
if ~exist('fds','var')
    fds = {'w_g', 'w0_g', 'w', 'w0', 'w2', 'w2_b'};   
end  
for i = 1:numel(fds)
    if isfield(params,fds{i}) &&  isfield(params,['d' fds{i}]) && ~isempty(params.(['d' fds{i}]))
        params.(fds{i}) = params.(fds{i}) - params.(['d' fds{i}]) * lr ./ batchsize;  
    end
    if isfield(params,[fds{i} '_mask'])
        params.(fds{i}) = params.(fds{i}) .* params.([fds{i} '_mask']);
    end
end



