function new_V = scramble_vector(V)
new_V = V;
z = randperm(numel(V));
new_V(:) = V(z);
