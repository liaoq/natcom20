function mnet = nnbox_update_w_bs(mnet,lr,batchsize) 
params = mnet.params; 
fds = {'f','b'};
for i = 1:numel(params)
    params(i) = monnlinal_update_w_bs(params(i),lr,batchsize,fds);  
    params(i) = monnlinal_empty_dw(params(i));
end

mnet.params = params;
