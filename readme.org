* Download Code Only

#+BEGIN_SRC
git clone https://gitlab.com/liaoq/natcom20.git
#+END_SRC

* Download Everything (Code and CIFAR-10 Dataset)

#+BEGIN_SRC
git clone --recursive https://gitlab.com/liaoq/natcom20.git
#+END_SRC

Delete .git folder to save some disk space since repo history is not needed.

* Requirements

Need the following compiled mex files from MatConvNet (put them in your Matlab path or under this folder)

vl_nnconv.mexa64

vl_nnbnorm.mexa64


I provide precompiled mexa64 files for GPU that should in principle work on Matlab 2018a and later versions (tested only with Ubuntu 16.04, Matlab 2018a, CUDA-8.0 and cudnn-8.0-v5.1 so far).     

You can compile your own mex files. These files appears in matlab/mex/ subfolder of MatConvNet after compilation. 

If you have trouble compiling them with GPU enabled, try the CPU-only option (will be much slower though).   

Ref: MatConvNet: https://www.vlfeat.org/matconvnet/


* Run Experiment

Run natcom20_fig1_experiment.m to reproduce experiments in fig1

In Matlab:

#+BEGIN_SRC
run natcom20_fig1_experiment.m
#+END_SRC

Figures will be saved in ~/natcom/fig1/rand_label_normalized/

* Dataset

cifar10_imdb/imdb.mat is the CIFAR-10 datset obtained from getCifarImdb.m provided by MatConvNet









