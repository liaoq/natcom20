function y = vl_linear(x,dy)
if ~exist('dy','var') || isempty(dy)
    y = x;
else
    y = dy;
end
