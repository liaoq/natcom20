function mnet = nnbox_absorb_bn(mnet)
% mnet_2 = mnet;  mnet_2 = nnbox_absorb_bn(mnet_2)           
% mnet.no_bn = 1;
for i = 1:numel(mnet.params)
    if  i ~= 1
        mmt = mnet.params(i).moments;
        mnet.params(i).f = mnet.params(i).f ./ permute(mmt(:,2),[2 3 1]);
        mnet.params(i).moments(:,2) = 1;      
        if i > 2  
            mnet.params(i-1).b = mnet.params(i-1).b - mmt(:,1);    
            %mnet.params(i-1).b = mnet.params(i-1).b + 0;      
            mnet.params(i).moments(:,1) = 0;     
            %mnet.params(i).moments(:,2) = 1;  
        end  
    end
end


% conv({},{})
% conv({A},{B},{C})
% conv(

