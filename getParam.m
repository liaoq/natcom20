function re = getParam (param,name,default) 
% if name is a field of param, return param.(name) 
% else return default value
if ( isstruct(param) && isfield(param, name) ) 
    re = getfield(param,name);
else 
    re = default;
end
