function C = cellfunApply (C,func,param1) 
if ~exist('param1','var')
    param1 = struct;
end
if ~iscell(param1)
    param1 = {param1};
end
counter = 0;
if ndims(C) == 1
    for d1 = 1:size(C,1)
        C{d1} = func(C{d1},param1{:});
        counter = counter+1;
        if mod(counter,500) == 0
            disp(['...  counter: ' func2str(func) ' ' num2str(counter)]);
        end
    end
elseif ndims(C) == 2
    for d1 = 1:size(C,1)
        for d2 = 1:size(C,2)
            C{d1,d2} = func(C{d1,d2},param1{:});
            counter = counter+1;
            if mod(counter,500) == 0
                disp(['...  counter: ' func2str(func) ' ' num2str(counter)]);
            end
        end
    end        
elseif ndims(C) == 3
    for d1 = 1:size(C,1)
        for d2 = 1:size(C,2)
            for d3 = 1:size(C,3)
                C{d1,d2,d3} = func(C{d1,d2,d3},param1{:});                
                counter = counter+1;
                if mod(counter,500) == 0
                    disp(['...  counter: ' func2str(func) ' ' num2str(counter)]);
                end
            end
        end
    end    
elseif ndims(C) == 4
    for d1 = 1:size(C,1)
        for d2 = 1:size(C,2)
            for d3 = 1:size(C,3)
                for d4 = 1:size(C,4)
                    C{d1,d2,d3,d4} = func(C{d1,d2,d3,d4},param1{:});                
                    counter = counter+1;
                    if mod(counter,500) == 0
                        disp(['...  counter: ' func2str(func) ' ' num2str(counter)]);
                    end
                end
            end
        end
    end
else % ndims(C) == 5
    error('not yet supported.');
end

