function mnet = nnbox_gather_loss_and_error(mnet)
fds = fieldnames(mnet);
for i = 1:numel(fds)  
    if ~isempty(regexp(fds{i}, 'loss')) || ~isempty(regexp(fds{i}, 'err')) 
        if isnumeric(mnet.(fds{i}))
            mnet.(fds{i}) = gather(mnet.(fds{i}));
        end
    end
end