function re = randElements(M,k)
% k random elements without replacement
y = randsample(numel(M),k);
re = M(y);
