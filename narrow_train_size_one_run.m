for e = 1:numel(learningRate)
    mnet.lr = learningRate(e);
    rand_seq = randperm(numel(train_idx));
    %rand_seq = 1:numel(train_idx);
    
    accum_error = 0;
    accum_loss  = 0;
    accum_num = 0;
    error_pattern = []; % zeros(size(rand_seq));
    for t = 0:batchsize:numel(rand_seq)-batchsize
        mnet.test_mode_p = 0;
        batch_idx = train_idx(rand_seq(t+1:t+batchsize));
        %batch_ims = nnbox_images_4D_random_shift(imdb.images.data(:,:,:,batch_idx,:),shift_range);
        batch_ims = imdb.images.data(:,:,:,batch_idx,:);
        %batch_ims = single(nnbox_images_4D_random_shift_and_rotate(uint8(imdb.images.data(:,:,:,batch_idx,:)),shift_range,rotate_range));
        mnet.x = batch_ims;
        mnet.y = mnet.labels(batch_idx);  % imdb.images.labels(batch_idx);  
        
        mnet.x = gpuArray(mnet.x); mnet.y = gpuArray(mnet.y);
        mnet = nnbox_simple_fnn_fp_and_bp(mnet); 
        
        %mnet = nnbox_update_w_bs_recy(mnet,mnet.lr,batchsize,recy_opts);  
        mnet = nnbox_update_w_bs(mnet,mnet.lr,batchsize);
        
        mnet = nnbox_gather_loss_and_error(mnet);
        accum_error = accum_error + mnet.top1err;
        accum_num   = accum_num + batchsize;
        accum_loss  = accum_loss + mnet.loss;
        
        progress_t = t/numel(rand_seq) + e - 1;
        disp(['Train epoch: ' num2str(progress_t) ' train loss avg: ' num2str(mnet.loss/batchsize) ' err: ' num2str(accum_error/accum_num) ]);
        
        error_pattern(rand_seq(t+1:t+batchsize)) = gather(mnet.error_pattern);
    end
    training_batch_error = accum_error/accum_num;
    training_batch_loss  = accum_loss/accum_num; 
    training_batch_error_pattern = error_pattern;
    %assert(training_batch_error == mean(training_batch_error_pattern));
    assert(single(training_batch_error) == single( mean(training_batch_error_pattern) ));
    
    stats.train(end+1,:) = gather([e accum_error/accum_num]);
    accum_error = 0;
    accum_num = 0;
    accum_loss = 0;
    %error_pattern = zeros(size(test_idx));
    error_pattern = [];
    
    for t_test = 0:batchsize:numel(test_idx)-batchsize
        mnet.test_mode_p = 1;
        batch_idx = test_idx(t_test+1:t_test+batchsize);
        % batch_idx = train_idx(rand_seq(t+1:t+batchsize)); 
        % batch_ims = nnbox_images_4D_random_shift(imdb.images.data(:,:,:,batch_idx,:),shift_range);
        %batch_ims = single(nnbox_images_4D_random_shift_and_rotate(uint8(imdb.images.data(:,:,:,batch_idx,:)),shift_range,rotate_range));
        batch_ims = single(imdb.images.data(:,:,:,batch_idx,:)); % no augmentation in test
        mnet.x = batch_ims; 
        mnet.y = mnet.labels(batch_idx); % imdb.images.labels(batch_idx); 
        mnet.x = gpuArray(mnet.x); mnet.y = gpuArray(mnet.y);
        mnet = nnbox_simple_fnn_fp(mnet);
        
        mnet = nnbox_gather_loss_and_error(mnet);
        %disp(mnet.top1err)
        accum_error = accum_error + mnet.top1err;
        accum_num   = accum_num + batchsize;
        accum_loss  = accum_loss + mnet.loss;
        disp(['testing .. '  num2str(t_test/numel(test_idx)) ' test loss avg: ' num2str(mnet.loss/batchsize)  ' err: ' num2str(accum_error/accum_num) ]);    
        
        error_pattern(t_test+1:t_test+batchsize) = gather(mnet.error_pattern); 
    end 
    stats.test(end+1,:) = [e accum_error/accum_num];
    
    test_batch_error = accum_error/accum_num;    
    test_batch_loss  = accum_loss/accum_num;    
    test_batch_error_pattern = error_pattern;
    
    %% record and perturbate
    mnet.training_batch_error = training_batch_error;
    mnet.training_batch_loss = training_batch_loss;
    mnet.training_batch_error_pattern = training_batch_error_pattern;
    
    mnet.test_batch_error = test_batch_error;    
    mnet.test_batch_loss  = test_batch_loss;     
    mnet.test_batch_error_pattern = test_batch_error_pattern;  
    
    % mnet.test_batch_error = test_batch_error;
    % recorded_models{end+1} = mnet;
    % if training_batch_error < 0.05
    %     disp('perturbate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    %     mnet = nnbox_perturbate_w(mnet,0.2);
    % end
    
end

  
  
  
  
