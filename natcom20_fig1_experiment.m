gpuDevice(1) % use the first gpu  


if ~exist('imdb','var') 
    imdb=load('cifar10_imdb/imdb.mat');
end

%% model
numClass = numel(imdb.meta.classes);
mnet = cifar_conv_model_sm_10000(numClass);
batchsize = 100;
%%

stats = struct;
stats.train = [];
stats.test = [];
test_counter = 0;
train_idx = find(imdb.images.set == 1);
test_idx = find(imdb.images.set == 3);
mnet.overwrite_params_during_backprop = 1;
learningRate = [ 0.1*ones(1,40) 0.01*ones(1,20)  0.001*ones(1,10) ]; 
mnet_back = mnet;
mmt = struct; 
shift_range= 5;
rotate_range = 0;
mnet.params = gpuArrayAllStruct(mnet.params);
mnet.no_bn = 0;
mnet.moments_avg_ratio =0;
recy_opts = struct;
mnet.keep_error_pattern = true;
mnet.labels = scramble_vector(imdb.images.labels);  
mnet_orig = mnet;

narrow_percent = [0.01 0.02:0.03:0.14 0.16 0.18 0.21 0.24 0.32 0.4:0.2:1];  
%narrow_percent = [1];
train_idx_orig = train_idx;
results = {};

for i = 1:numel(narrow_percent)
    stats = struct;    
    stats.train = [];
    stats.test = [];

    % train_idx = randElements( train_idx_orig,  round( narrow_percent(i) * numel(train_idx_orig) ) );
    % train_idx = randElements_re( train_idx, numel(train_idx_orig) );
    narrow_number =   round( narrow_percent(i) * numel(train_idx_orig) );
    train_idx = randElements( train_idx_orig, narrow_number  );      
    train_idx = [train_idx  randElements_re( train_idx, numel(train_idx_orig) - numel(train_idx) )];     
    assert(numel(unique(train_idx)) == narrow_number );
    current_train_idx = train_idx;

    mnet = mnet_orig;
    narrow_train_size_one_run
    results{i,1} = stats.train;
    results{i,2} = stats.test;

    %clear stats
    model_points{i} = mnet;    
    model_points{i}.params = gatherAllStruct(model_points{i}.params);      
    model_points{i}.train_idx = current_train_idx;      
    
end
if ~exist('results_orig','var')
    results_orig = results;
end
 
% % fix results
% for i = 1:size(results,1)    
%     for jj = 1:2
%         results{i,jj} = results{i,jj}(end-numel(learningRate)+1:end,:); 
%     end
% end
% %
      
results_reshape = results;         
for i = 1:size(results_reshape,1)
    for jj = 1:size(results_reshape,2)
        results_reshape{i,jj} = permute(results_reshape{i,jj},[1 3 2]);
    end
end

 
%% l2 
model_absorb_points = model_points;         
for i = 1:numel(model_absorb_points) 
    model_absorb_points{i} = nnbox_absorb_bn(model_absorb_points{i});    
    model_absorb_points{i} = nnbox_normalize_weights_by_l2(model_absorb_points{i});        
    model_absorb_points{i}.params = gpuArrayAllStruct(model_absorb_points{i}.params);
    model_absorb_points{i} =  nnbox_eval_on_test_set(model_absorb_points{i},imdb)  ; 
    %model_absorb_points{i} =  nnbox_eval_on_train_set_with_test_moments_narrow_idx(model_absorb_points{i},imdb,model_absorb_points{i}.train_idx);      
    model_absorb_points{i} = nnbox_eval_on_train_set_w_test_mmts_and_mnetlabels_narrow(model_absorb_points{i},imdb,model_absorb_points{i}.train_idx);  
    model_absorb_points{i}.params = gatherAllStruct(model_absorb_points{i}.params); 
end  
         
%% orig
model_orig_points = model_points;  
for i = 1:numel(model_orig_points) 
    model_orig_points{i} = nnbox_absorb_bn(model_orig_points{i});    
    %model_orig_points{i} = nnbox_normalize_weights_by_average_l2_const(model_orig_points{i});
    model_orig_points{i}.params = gpuArrayAllStruct(model_orig_points{i}.params);
    model_orig_points{i} =  nnbox_eval_on_test_set(model_orig_points{i},imdb)  ; 
    %model_orig_points{i} =  nnbox_eval_on_train_set_with_test_moments_narrow_idx(model_orig_points{i},imdb,model_orig_points{i}.train_idx) ;  
    model_orig_points{i} = nnbox_eval_on_train_set_w_test_mmts_and_mnetlabels_narrow(model_orig_points{i},imdb,model_orig_points{i}.train_idx) ;    
    %model_orig_points{i} =  nnbox_eval_on_train_set_with_training_mode_2_narrow_idx(model_orig_points{i},imdb,model_orig_points{i}.train_idx) ;  
    model_orig_points{i}.params = gatherAllStruct(model_orig_points{i}.params);     
end 
    
 
  
% train_err =  reshape(stats.train,[],numel(narrow_percent),2);   
% test_err =  reshape(stats.test,[],numel(narrow_percent),2);     
% close all;
% suptitle(['Legends are training set sizes, model #params: ' num2str(sum(num_params(:))) ]);  
% subplot(1,2,1);
% multicurve_plot_with_sym_simple(1:size(train_err,1),train_err(:,:,2)');
% legend(num_data);grid on;
% xlabel('Training Epoch (each with 50000 samples)');ylabel('Training Error on CIFAR-10');
% ylim([0 0.7]);
% subplot(1,2,2);
% multicurve_plot_with_sym_simple(1:size(test_err,1),test_err(:,:,2)');
% legend(num_data);grid on;
% xlabel('Training Epoch (each with 50000 samples)');ylabel('Validation Error on CIFAR-10');
% ylim([0.3 0.8]);
% filepath = fullfile(['~/natcom/fig1/narrow_percent_num_' num2str(numel(narrow_percent)) '.jpg'] );   
% mkdirFile(filepath);
% print_jpg_new_full_screen (filepath,gcf);
% print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf);
% save(strrep(filepath,'.jpg','.mat'),'results','mnet');
 

num_data = num2cellStrArray(narrow_percent*numel(train_idx),'');  
num_params = nnbox_count_num_params(mnet)
  
close all;        
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);     
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_error;
    test_err(1,i)  = model_absorb_points{i}.test_batch_error; 
end
plot(round(narrow_percent*numel(train_idx)),train_err,'-o')   
hold all;
plot(round(narrow_percent*numel(train_idx)),test_err,'-*')     
hold all;
plot(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_error.jpg'] ); 
legend('Training Error','Test Error','Test-Training Error Difference'); 
xlabel('Number of Training Examples');
ylabel('Error on CIFAR-10');  
vline(sum(num_params(:)),'--g','Model #Params')  
    
mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
%save(strrep(filepath,'.jpg','.mat'),'results','mnet');
   
  
close all;  
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);     
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_loss;
    test_err(1,i)  = model_absorb_points{i}.test_batch_loss; 
end
plot(round(narrow_percent*numel(train_idx)),train_err,'-o')   
hold all;
plot(round(narrow_percent*numel(train_idx)),test_err,'-*')     
hold all;
%plot(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_loss.jpg'] ); 
%legend('Training Loss','Test Loss','Test-Training Loss Difference'); 
legend('Training Loss','Test Loss'); 
xlabel('Number of Training Examples');
ylabel('Loss on CIFAR-10');  
vline(sum(num_params(:)),'--g','Model #Params')  
    
mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
%save(strrep(filepath,'.jpg','.mat'),'results','mnet');
  
%%%%%%%%%%%%%%% 
close all;        
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);    
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_error;
    test_err(1,i)  = model_absorb_points{i}.test_batch_error; 
end
semilogx(round(narrow_percent*numel(train_idx)),train_err,'-o')    
hold all;
semilogx(round(narrow_percent*numel(train_idx)),test_err,'-*')      
hold all;
semilogx(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_error_logx.jpg'] ); 
legend('Training Error','Test Error','Test-Training Error Difference'); 
xlabel('Number of Training Examples');
ylabel('Error on CIFAR-10');  
vline(sum(num_params(:)),'--g','Model #Params')      
mkdirFile(filepath);    
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
%save(strrep(filepath,'.jpg','.mat'),'results','mnet');  


close all;  
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);     
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_orig_points{i}.training_batch_loss;
    test_err(1,i)  = model_orig_points{i}.test_batch_loss; 
end
semilogx(round(narrow_percent*numel(train_idx)),train_err,'-o')   
hold all;
semilogx(round(narrow_percent*numel(train_idx)),test_err,'-*')     
hold all;
%semilogx(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_unorm_loss_logx.jpg'] ); 
%legend('Training Loss','Test Loss','Test-Training Loss Difference');     
legend('Training Loss','Test Loss'); 
xlabel('Number of Training Examples');
ylabel('Loss on CIFAR-10 (Unormalized Network)');  
vline(sum(num_params(:)),'--g','Model #Params')  
   
mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 

 
close all;  
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);     
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_loss;
    test_err(1,i)  = model_absorb_points{i}.test_batch_loss; 
end
semilogx(round(narrow_percent*numel(train_idx)),train_err,'-o')   
hold all;
semilogx(round(narrow_percent*numel(train_idx)),test_err,'-*')     
hold all;
%semilogx(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_normalized_loss_logx.jpg'] ); 
%legend('Training Loss','Test Loss','Test-Training Loss Difference');     
legend('Training Loss','Test Loss'); 
xlabel('Number of Training Examples');
ylabel('Loss on CIFAR-10 (Normalized Network)');  
vline(sum(num_params(:)),'--g','Model #Params')  
   
mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
%save(strrep(filepath,'.jpg','.mat'),'results','mnet');
 

close all;  
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);     
train_err = [];
test_err = [];
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_loss;
    test_err(1,i)  = model_absorb_points{i}.test_batch_loss; 
end
semilogx(round(narrow_percent*numel(train_idx)),train_err,'-o')   
hold all;
semilogx(round(narrow_percent*numel(train_idx)),test_err,'-*')     
hold all;
semilogx(round(narrow_percent*numel(train_idx)),abs(test_err - train_err),'-p') 
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_normalized_loss_logx_with_diff.jpg'] ); 
legend('Training Loss','Test Loss','Test-Training Loss Difference');     
%legend('Training Loss','Test Loss'); 
xlabel('Number of Training Examples');
ylabel('Loss on CIFAR-10 (Normalized Network)');  
vline(sum(num_params(:)),'--g','Model #Params')  
   
mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
%save(strrep(filepath,'.jpg','.mat'),'results','mnet'); 
   
%%% diff only
 close all;    
suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);    
train_err = [];     
test_err = [];    
for i = 1:numel(model_absorb_points)
    train_err(1,i) = model_absorb_points{i}.training_batch_loss;
    test_err(1,i)  = model_absorb_points{i}.test_batch_loss; 
end
ntrain_points = round(narrow_percent*numel(train_idx));  
% semilogx(ntrain_points,train_err,'-o')   
% hold all;
% semilogx(ntrain_points,test_err,'-*')     
% hold all;
test_train_diff = abs(test_err - train_err);   
semilogx(ntrain_points, test_train_diff,'-p')  
filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_normalized_loss_diff_only.jpg']);                         
%legend('Training Loss','Test Loss','Test-Training Loss Difference');     
%legend('Training Loss','Test Loss');    
xlabel('Number of Training Examples');
ylabel('Loss on CIFAR-10 (Normalized Network)');  
vline(sum(num_params(:)),'--g','Model #Params')            
%save(strrep(filepath,'.jpg','.mat'),'results','mnet'); 
% fit linear 
num_points_to_fit = 7;
log10_ntrain_points = log10(ntrain_points)
% log10_ntrain_points(end-num_points_to_fit+1:end)
% test_train_diff(end-num_points_to_fit+1:end) 
p = polyfit(log10_ntrain_points(end-num_points_to_fit+1:end) ,test_train_diff(end-num_points_to_fit+1:end) ,1)   

line_x_log10 = floor(log10_ntrain_points(1)):0.1:ceil(log10_ntrain_points(end));   
line_y = polyval(p,line_x_log10);

line_x = 10.^line_x_log10;

hold all;
semilogx(line_x,line_y ,'--')  

txt1 = [ '  y = ' num2str(p(1)) '*x + ' num2str(p(2)) ' (log scale) ' ] ; 
text(line_x(1),line_y(1),txt1)
    
legend('Test-Training Loss Difference', [ 'Linear Fit of Last ' num2str(num_points_to_fit) ' Points']);         

mkdirFile(filepath); 
print_jpg_new_full_screen (filepath,gcf); 
print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
   



% %%%%%%%%%%%%%%%%%%
 
% close all;
% suptitle(['Model #params: ' num2str(sum(num_params(:))) ]);  
% semilogx(round(narrow_percent*numel(train_idx)),train_err(end,:,2),'-o')
% hold all;
% semilogx(round(narrow_percent*numel(train_idx)),test_err(end,:,2),'-*') 
% hold all;
% semilogx(round(narrow_percent*numel(train_idx)),test_err(end,:,2) - train_err(end,:,2),'-p') 
% filepath = fullfile(['~/natcom/fig1/rand_label_normalized/narrow_percent_num_' num2str(numel(narrow_percent)) '_view2_logx.jpg'] );   
% %legend('Training Error','Test Error','Test-Training Error Difference'); 
% legend('Training Error','Test Error','Test-Training Error Difference','location','SouthWest'); 
% xlabel('Number of Training Examples');
% ylabel('Error on CIFAR-10');  
% vline(sum(num_params(:)),'--g','Model #Params') 
% mkdirFile(filepath); 
% print_jpg_new_full_screen (filepath,gcf); 
% print_eps_new_full_screen (strrep(filepath,'.jpg','.eps'),gcf); 
