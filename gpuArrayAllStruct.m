function re = gpuArrayAllStruct (input,dummy)
% takes in any input, checks type, and transfer all the data to gpuArray
if isstr(input)
    re = input; % do not change string
elseif isnumeric(input) || islogical(input)
    re = gpuArray(input);
elseif iscell(input)
    re = cellfunApply (input,@gpuArrayAll); % apply to each element of the cell 
elseif isstruct(input)
    if numel(input) == 1
        fds = fieldnames(input);
        for f = 1:numel(fds)
            input(1).(fds{f}) = gpuArrayAllStruct(input(1).(fds{f}));
        end
    else
        for i = 1:numel(input)
            input(i) = gpuArrayAllStruct(input(i));
        end
    end
    re = input;
    % if isfield(input,'gpuArray_func')
    %     re = input.gpuArray_func(input);
    % else
    %     error('gpuArrayAll:  the input struct should provide a customized gpuArray_func');
    % end
else
    re = input;
    %disp('gpuArrayAllStruct: type not recognized/supported.'); 
end
