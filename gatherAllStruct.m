function re = gatherAllStruct (input,dummy)
% takes in any input, checks type, and transfer all the data to cpu
if isstr(input)
    re = input; % do not change string
elseif isnumeric(input) || islogical(input)
    re = gather(input);
elseif iscell(input)
    re = cellfunApply (input,@gatherAllStruct); % apply to each element of the cell 
elseif isstruct(input)
    if numel(input) == 1
        fds = fieldnames(input);
        for f = 1:numel(fds)
            input(1).(fds{f}) = gatherAllStruct(input(1).(fds{f}));
        end
    else
        for i = 1:numel(input)
            input(i) = gatherAllStruct(input(i));
        end
    end
    re = input;
    % if isfield(input,'gather_func')
    %     re = input.gather_func(input);
    % else
    %     error('gatherAll:  the input struct should provide a customized gather_func');
    % end
else
    re = input;
    %disp('gatherAllStruct: type not recognized/supported.');  
end



