function mnet =  nnbox_eval_on_train_set_w_test_mmts_and_mnetlabels_narrow(mnet,imdb,training_idx)      
%% imdb labels are not used    

%   mnet_2 =  nnbox_eval_on_test_set(mnet_2,imdb)   
batchsize = 100;     

%train_idx = find(imdb.images.set == 1);
%test_idx = find(imdb.images.set == 3);
%training_idx = find(imdb.images.set == 1);                 
  

accum_error = 0;
accum_loss  = 0;
accum_num = 0;
error_pattern = [];

% orig_labels = mnet.labels;
% try 
%     mnet.labels = imdb.images.label;
% catch
%     mnet.labels = imdb.images.labels;
% end    

for t_test = 0:batchsize:numel(training_idx)-batchsize
    mnet.training_mode_p = 1;
    batch_idx = training_idx(t_test+1:t_test+batchsize);
    % batch_idx = training_idx(rand_seq(t+1:t+batchsize)); 
    % batch_ims = nnbox_images_4D_random_shift(imdb.images.data(:,:,:,batch_idx,:),shift_range);
    %batch_ims = single(nnbox_images_4D_random_shift_and_rotate(uint8(imdb.images.data(:,:,:,batch_idx,:)),shift_range,rotate_range));
    batch_ims = single(imdb.images.data(:,:,:,batch_idx,:)); % no augmentation in test
    mnet.x = batch_ims; 
    mnet.y = mnet.labels(batch_idx);
    mnet.x = gpuArray(mnet.x); mnet.y = gpuArray(mnet.y);
    mnet = nnbox_simple_fnn_fp(mnet);
    
    mnet = nnbox_gather_loss_and_error(mnet);
    %disp(mnet.top1err)
    accum_error = accum_error + mnet.top1err;
    accum_loss = accum_loss   + mnet.loss;
    accum_num   = accum_num + batchsize;
    disp(['testing .. '  num2str(t_test/numel(training_idx)) ' mini-batch loss: ' num2str(mnet.loss)  ' total loss: ' num2str(accum_loss/accum_num)  ' err: ' num2str(accum_error/accum_num) ]);
    
    error_pattern(t_test+1:t_test+batchsize) = gather( mnet.error_pattern );
end
%stats.test(end+1,:) = [e accum_error/accum_num];

training_batch_error = accum_error/accum_num;    
training_batch_loss  = accum_loss/accum_num;
training_batch_error_pattern = error_pattern;

mnet.training_batch_error = training_batch_error;
mnet.training_batch_loss = training_batch_loss;
mnet.training_batch_error_pattern = gather( training_batch_error_pattern );

     

% mnet.labels = orig_labels;    
