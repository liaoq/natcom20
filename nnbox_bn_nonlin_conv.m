function [mnet layers params] = nnbox_bn_nonlin_conv(mnet,layers,params,layer_idx,bprop_p)
% [y df db moments layer_opts] = nnbox_bn_nonlin_conv(opts,nonlin,convOpts,x,f,b,moments,dy,layer_opts) 

nonlin = layers(layer_idx).nonlinear; 
convOpts = layers(layer_idx).convOpts;
x = layers(layer_idx-1).x;
f = params(layer_idx).f;
b = params(layer_idx).b;

if bprop_p
    dy = layers(layer_idx).dx;
else
    dy = [];
end
if isfield(mnet,'test_mode_p') && mnet.test_mode_p
    moments = params(layer_idx).moments;
else
    moments = [];
end


% no param for bn
if isfield(mnet,'no_bn') && mnet.no_bn
    x_bn = x;
else
    bnG = gather(x(1,1,:,1))*0+1; bnG = gpuArray( bnG(:) ); 
    bnB = gather(x(1,1,:,1))*0; bnB = gpuArray( bnB(:) ); 
    if exist('moments','var') && ~isempty(moments)
        x_bn = vl_nnbnorm(x,bnG,bnB,'Moments',moments);
    else
        x_bn = vl_nnbnorm(x,bnG,bnB);
    end
end
x_nonlin = nonlin(x_bn);


if ~exist('dy','var') || isempty(dy)
    % forward 
    y = vl_nnconv(x_nonlin,f,b,convOpts{:});
    layers(layer_idx).x = y;
    
else
    [dx_nonlin df db] = vl_nnconv(x_nonlin,f,b,dy,convOpts{:});
    dx_bn = nonlin(x_bn,dx_nonlin);
    if isfield(mnet,'no_bn') && mnet.no_bn
        dx = dx_bn;
        moments = [];
    else
        [dx trash1 trash2 moments] = vl_nnbnorm(x,bnG,bnB,dx_bn);
        params(layer_idx) = nnbox_exp_avg_field(mnet.moments_avg_ratio,params(layer_idx),'moments',moments);
    end
    
    
    layers(layer_idx-1).dx = dx;
    params(layer_idx).df = df;
    params(layer_idx).db = db;
    
end


