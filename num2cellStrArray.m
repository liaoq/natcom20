function re = num2cellStrArray (A,replace_space)
re = cellstr(num2str(A));
if (size(re,1) == 1 && size(re,2) == 1)
    re = cellstr(num2str(A'));
end
if exist('replace_space','var')
    re =  strrepcell(re,' ',replace_space);
end

function re = strrepcell (I,A,B)
re =  cellfun(@(x) strrep(x,A,B),I, 'UniformOutput',false );

