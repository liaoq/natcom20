function mnet = nnbox_simple_fnn_fp(mnet) 
mnet.layers(1).x = mnet.x;   
mnet = nnbox_simple_fnn(mnet,0);

if isfield(mnet,'y') && ~isempty(mnet.y)
    mnet.loss       = vl_nnloss(mnet.layers(end).x,mnet.y,[],'loss','softmaxlog');  
    mnet.top1err    = vl_nnloss(mnet.layers(end).x,mnet.y,[],'loss','topkerror','topK',1);

    outputval = mnet.layers(end).x;  

    
    if isfield(mnet,'keep_error_pattern') && mnet.keep_error_pattern
        [~, pred] = max(mnet.layers(end).x,[],3);
        mnet.error_pattern = (pred(:) ~= mnet.y(:) );
    end
end





function compute_loss(x)
  

% Xmax = max(x,[],3) ;
% ex = exp(bsxfun(@minus, x, Xmax)) ;
% y = bsxfun(@rdivide, ex, sum(ex,3)) ;

